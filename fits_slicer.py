import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import sys

def slice_fits(data, w, xmin, xmax, ymin, ymax, file_out, hist):
    """ Cuts out xmin:xmax,ymin:ymax subarray from data and
    adjust wcs accordingly and writes out to file file file_out
    """

    dcut = data[xmin:xmax, ymin:ymax]
    wcut = w[xmin:xmax, ymin:ymax]
    head = wcut.to_header()
    PriHdu = fits.PrimaryHDU(header=head,data=dcut)
    for h in hist:
        PriHdu.header['HISTORY'] = h
    PriHdu.writeto(file_out)



###################################################################
#
# Main Prog
#
# eg call
#
# python fits_slice Mosaic.fits 4 100 Mosaic_Split
#
# will split Mosaic.fits into 16 subfile 4x4 with a 100 pixel overlap
# on the internal boarders

fits_file = sys.argv[1]    # Input map
nside = int(sys.argv[2])   # Split nside times along an axis
ovlap = int(sys.argv[3])   # Number of pixels to overlap at edges
oname = sys.argv[4]        # Output prefix, will add map number

hdu = fits.open(fits_file)
data = hdu[0].data
data.shape = (data.shape[2],data.shape[3])
hist = hdu[0].header['HISTORY']

w = WCS(hdu[0].header)
w = w.dropaxis(2)
w = w.dropaxis(2)

nxpix = data.shape[0]/nside
nypix = data.shape[1]/nside

nmap = 0
for i in range(nside):

    xmin = i*nxpix - ovlap/2
    if xmin<1: xmin = 1

    xmax = (i+1)*nxpix + ovlap/2
    if xmax>data.shape[0]: xmax = data.shape[0]

    for j in range(nside):

        ymin = j*nypix - ovlap/2
        if ymin<1: ymin = 1

        ymax = (j+1)*nypix + ovlap/2
        if ymax>data.shape[1]: ymax = data.shape[1]
        fname = "%s_%3.3i.fits" % (oname,nmap)

        print "Writing",fname
        slice_fits(data,w,xmin,xmax,ymin,ymax,fname,hist)

        nmap += 1

print "Finished"