# README #

* Python script to slice up a large fits image produced by aips
* Version 1.0

### Setup

* Should run using anaconda python
* Dependencies - uses astropy fits and wcs
* Use python slice_fits.py <input_fits> <nside> <pix_overlap> <output_file_prefix>

Will slice <input_fits> image into <nside> x <nside> sub_images with <pix_overlap> pixel overlap
between internal borders. This is to allow source finders like pybdsf to pick up source which may 
near the edge of a map (NB this may mean some sources are found twice). The sub image are writen to
a the give <output_file_prefix> followed by a three digit number in the form "_000.fits".

